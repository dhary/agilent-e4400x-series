﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="11008008">
	<Property Name="NI.Lib.Icon" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91P&lt;!FNA#^M#5Y6M96NA"R[WM#WQ"&lt;9A0ZYR'E?G!WPM1$AN&gt;@S(!ZZQG&amp;0%VLZ'@)H8:_X\&lt;^P(^7@8H\4Y;"`NX\;8JZPUX@@MJXC]C.3I6K5S(F/^DHTE)R`ZS%@?]J;XP/5N&lt;XH*3V\SEJ?]Z#F0?=J4HP+5&lt;Y=]Z#%0/&gt;+9@%QU"BU$D-YI-4[':XC':XB]D?%:HO%:HO(2*9:H?):H?)&lt;(&lt;4%]QT-]QT-]BNIEMRVSHO%R@$20]T20]T30+;.Z'K".VA:OAW"%O^B/GK&gt;ZGM&gt;J.%`T.%`T.)`,U4T.UTT.UTROW6;F.]XDE0-9*IKH?)KH?)L(U&amp;%]R6-]R6-]JIPC+:[#+"/7Q2'CX&amp;1[F#`&amp;5TR_2@%54`%54`'YN$WBWF&lt;GI8E==J\E3:\E3:\E-51E4`)E4`)EDW%D?:)H?:)H?5Q6S:-]S:-A;6,42RIMX:A[J3"Z`'S\*&lt;?HV*MENS.C&lt;&gt;Z9GT,7:IOVC7*NDFA00&gt;&lt;$D0719CV_L%7.N6CR&amp;C(7(R=,(1M4;Z*9.T][RNXH46X62:X632X61?X6\H(L8_ZYP^`D&gt;LP&amp;^8K.S_53Z`-Z4K&gt;4()`(/"Q/M&gt;`P9\@&lt;P&lt;U'PDH?8AA`XUMPTP_EXOF`[8`Q&lt;IT0]?OYVOA(5/(_Z!!!!!!</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="Private" Type="Folder">
		<Item Name="hpesgdxx Utility Check Options.vi" Type="VI" URL="../Private/hpesgdxx Utility Check Options.vi"/>
		<Item Name="hpesgdxx Utility Clean Up Initialize.vi" Type="VI" URL="../Private/hpesgdxx Utility Clean Up Initialize.vi"/>
		<Item Name="hpesgdxx Utility Create Binary String.vi" Type="VI" URL="../Private/hpesgdxx Utility Create Binary String.vi"/>
		<Item Name="hpesgdxx Utility Default Instrument Setup.vi" Type="VI" URL="../Private/hpesgdxx Utility Default Instrument Setup.vi"/>
		<Item Name="hpesgdxx Utility Parse Binary String.vi" Type="VI" URL="../Private/hpesgdxx Utility Parse Binary String.vi"/>
	</Item>
	<Item Name="Public" Type="Folder">
		<Item Name="hpesgdxx Abort.vi" Type="VI" URL="../Public/hpesgdxx Abort.vi"/>
		<Item Name="hpesgdxx ALC Or Power Search.vi" Type="VI" URL="../Public/hpesgdxx ALC Or Power Search.vi"/>
		<Item Name="hpesgdxx BERT Measurement Result.vi" Type="VI" URL="../Public/hpesgdxx BERT Measurement Result.vi"/>
		<Item Name="hpesgdxx BERT On_Off.vi" Type="VI" URL="../Public/hpesgdxx BERT On_Off.vi"/>
		<Item Name="hpesgdxx CDMA On_Off.vi" Type="VI" URL="../Public/hpesgdxx CDMA On_Off.vi"/>
		<Item Name="hpesgdxx CDMA2000 On_Off.vi" Type="VI" URL="../Public/hpesgdxx CDMA2000 On_Off.vi"/>
		<Item Name="hpesgdxx Check Info.vi" Type="VI" URL="../Public/hpesgdxx Check Info.vi"/>
		<Item Name="hpesgdxx Circuit Protection.vi" Type="VI" URL="../Public/hpesgdxx Circuit Protection.vi"/>
		<Item Name="hpesgdxx Clear Header.vi" Type="VI" URL="../Public/hpesgdxx Clear Header.vi"/>
		<Item Name="hpesgdxx Clear Markers.vi" Type="VI" URL="../Public/hpesgdxx Clear Markers.vi"/>
		<Item Name="hpesgdxx Close.vi" Type="VI" URL="../Public/hpesgdxx Close.vi"/>
		<Item Name="hpesgdxx Config ALC Search Span.vi" Type="VI" URL="../Public/hpesgdxx Config ALC Search Span.vi"/>
		<Item Name="hpesgdxx Config Alternate Amplitude.vi" Type="VI" URL="../Public/hpesgdxx Config Alternate Amplitude.vi"/>
		<Item Name="hpesgdxx Config AM Modulation.vi" Type="VI" URL="../Public/hpesgdxx Config AM Modulation.vi"/>
		<Item Name="hpesgdxx Config AM Step Increment.vi" Type="VI" URL="../Public/hpesgdxx Config AM Step Increment.vi"/>
		<Item Name="hpesgdxx Config Amplitude.vi" Type="VI" URL="../Public/hpesgdxx Config Amplitude.vi"/>
		<Item Name="hpesgdxx Config ARB High Crest Mode.vi" Type="VI" URL="../Public/hpesgdxx Config ARB High Crest Mode.vi"/>
		<Item Name="hpesgdxx Config ARB IQ Modulation.vi" Type="VI" URL="../Public/hpesgdxx Config ARB IQ Modulation.vi"/>
		<Item Name="hpesgdxx Config ARB Marker Polarity.vi" Type="VI" URL="../Public/hpesgdxx Config ARB Marker Polarity.vi"/>
		<Item Name="hpesgdxx Config ARB Scaling.vi" Type="VI" URL="../Public/hpesgdxx Config ARB Scaling.vi"/>
		<Item Name="hpesgdxx Config ARB Segment Trigger.vi" Type="VI" URL="../Public/hpesgdxx Config ARB Segment Trigger.vi"/>
		<Item Name="hpesgdxx Config BERT Comparator.vi" Type="VI" URL="../Public/hpesgdxx Config BERT Comparator.vi"/>
		<Item Name="hpesgdxx Config BERT Data.vi" Type="VI" URL="../Public/hpesgdxx Config BERT Data.vi"/>
		<Item Name="hpesgdxx Config BERT Display.vi" Type="VI" URL="../Public/hpesgdxx Config BERT Display.vi"/>
		<Item Name="hpesgdxx Config BERT Input.vi" Type="VI" URL="../Public/hpesgdxx Config BERT Input.vi"/>
		<Item Name="hpesgdxx Config BERT Trigger.vi" Type="VI" URL="../Public/hpesgdxx Config BERT Trigger.vi"/>
		<Item Name="hpesgdxx Config CDMA Filter.vi" Type="VI" URL="../Public/hpesgdxx Config CDMA Filter.vi"/>
		<Item Name="hpesgdxx Config CDMA IQ Mapping.vi" Type="VI" URL="../Public/hpesgdxx Config CDMA IQ Mapping.vi"/>
		<Item Name="hpesgdxx Config CDMA Trigger.vi" Type="VI" URL="../Public/hpesgdxx Config CDMA Trigger.vi"/>
		<Item Name="hpesgdxx Config CDMA.vi" Type="VI" URL="../Public/hpesgdxx Config CDMA.vi"/>
		<Item Name="hpesgdxx Config CDMA2000 Filter.vi" Type="VI" URL="../Public/hpesgdxx Config CDMA2000 Filter.vi"/>
		<Item Name="hpesgdxx Config CDMA2000 Forward.vi" Type="VI" URL="../Public/hpesgdxx Config CDMA2000 Forward.vi"/>
		<Item Name="hpesgdxx Config CDMA2000 IQ Mapping.vi" Type="VI" URL="../Public/hpesgdxx Config CDMA2000 IQ Mapping.vi"/>
		<Item Name="hpesgdxx Config CDMA2000 Reverse.vi" Type="VI" URL="../Public/hpesgdxx Config CDMA2000 Reverse.vi"/>
		<Item Name="hpesgdxx Config CDMA2000 Spreading.vi" Type="VI" URL="../Public/hpesgdxx Config CDMA2000 Spreading.vi"/>
		<Item Name="hpesgdxx Config CDMA2000 Trigger.vi" Type="VI" URL="../Public/hpesgdxx Config CDMA2000 Trigger.vi"/>
		<Item Name="hpesgdxx Config Clipping.vi" Type="VI" URL="../Public/hpesgdxx Config Clipping.vi"/>
		<Item Name="hpesgdxx Config Continuous Trigger.vi" Type="VI" URL="../Public/hpesgdxx Config Continuous Trigger.vi"/>
		<Item Name="hpesgdxx Config Dual ARB Trigger.vi" Type="VI" URL="../Public/hpesgdxx Config Dual ARB Trigger.vi"/>
		<Item Name="hpesgdxx Config Dual ARB Waveform.vi" Type="VI" URL="../Public/hpesgdxx Config Dual ARB Waveform.vi"/>
		<Item Name="hpesgdxx Config Dual ARB.vi" Type="VI" URL="../Public/hpesgdxx Config Dual ARB.vi"/>
		<Item Name="hpesgdxx Config External Reference.vi" Type="VI" URL="../Public/hpesgdxx Config External Reference.vi"/>
		<Item Name="hpesgdxx Config External Trigger.vi" Type="VI" URL="../Public/hpesgdxx Config External Trigger.vi"/>
		<Item Name="hpesgdxx Config FM Modulation.vi" Type="VI" URL="../Public/hpesgdxx Config FM Modulation.vi"/>
		<Item Name="hpesgdxx Config FM Signal Example.vi" Type="VI" URL="../Public/hpesgdxx Config FM Signal Example.vi"/>
		<Item Name="hpesgdxx Config FM Step Increment.vi" Type="VI" URL="../Public/hpesgdxx Config FM Step Increment.vi"/>
		<Item Name="hpesgdxx Config Frequency Channel.vi" Type="VI" URL="../Public/hpesgdxx Config Frequency Channel.vi"/>
		<Item Name="hpesgdxx Config Frequency.vi" Type="VI" URL="../Public/hpesgdxx Config Frequency.vi"/>
		<Item Name="hpesgdxx Config GSM Framed Timeslot.vi" Type="VI" URL="../Public/hpesgdxx Config GSM Framed Timeslot.vi"/>
		<Item Name="hpesgdxx Config GSM Framed.vi" Type="VI" URL="../Public/hpesgdxx Config GSM Framed.vi"/>
		<Item Name="hpesgdxx Config GSM Pattern.vi" Type="VI" URL="../Public/hpesgdxx Config GSM Pattern.vi"/>
		<Item Name="hpesgdxx Config IQ Adjustments External.vi" Type="VI" URL="../Public/hpesgdxx Config IQ Adjustments External.vi"/>
		<Item Name="hpesgdxx Config IQ Adjustments.vi" Type="VI" URL="../Public/hpesgdxx Config IQ Adjustments.vi"/>
		<Item Name="hpesgdxx Config IQ Calibration.vi" Type="VI" URL="../Public/hpesgdxx Config IQ Calibration.vi"/>
		<Item Name="hpesgdxx Config IQ Modulation.vi" Type="VI" URL="../Public/hpesgdxx Config IQ Modulation.vi"/>
		<Item Name="hpesgdxx Config IQ Skew Correction.vi" Type="VI" URL="../Public/hpesgdxx Config IQ Skew Correction.vi"/>
		<Item Name="hpesgdxx Config List Sweep.vi" Type="VI" URL="../Public/hpesgdxx Config List Sweep.vi"/>
		<Item Name="hpesgdxx Config Low Frequency Output.vi" Type="VI" URL="../Public/hpesgdxx Config Low Frequency Output.vi"/>
		<Item Name="hpesgdxx Config Marker Destination.vi" Type="VI" URL="../Public/hpesgdxx Config Marker Destination.vi"/>
		<Item Name="hpesgdxx Config Marker Polarity.vi" Type="VI" URL="../Public/hpesgdxx Config Marker Polarity.vi"/>
		<Item Name="hpesgdxx Config Marker to Blank.vi" Type="VI" URL="../Public/hpesgdxx Config Marker to Blank.vi"/>
		<Item Name="hpesgdxx Config NADC Framed Timeslot.vi" Type="VI" URL="../Public/hpesgdxx Config NADC Framed Timeslot.vi"/>
		<Item Name="hpesgdxx Config NADC Framed.vi" Type="VI" URL="../Public/hpesgdxx Config NADC Framed.vi"/>
		<Item Name="hpesgdxx Config NADC Pattern.vi" Type="VI" URL="../Public/hpesgdxx Config NADC Pattern.vi"/>
		<Item Name="hpesgdxx Config NADC Signal Example.vi" Type="VI" URL="../Public/hpesgdxx Config NADC Signal Example.vi"/>
		<Item Name="hpesgdxx Config PDC Framed Timeslot.vi" Type="VI" URL="../Public/hpesgdxx Config PDC Framed Timeslot.vi"/>
		<Item Name="hpesgdxx Config PDC Framed.vi" Type="VI" URL="../Public/hpesgdxx Config PDC Framed.vi"/>
		<Item Name="hpesgdxx Config PDC Pattern.vi" Type="VI" URL="../Public/hpesgdxx Config PDC Pattern.vi"/>
		<Item Name="hpesgdxx Config PM Modulation.vi" Type="VI" URL="../Public/hpesgdxx Config PM Modulation.vi"/>
		<Item Name="hpesgdxx Config PM Step Increment.vi" Type="VI" URL="../Public/hpesgdxx Config PM Step Increment.vi"/>
		<Item Name="hpesgdxx Config Pulse Modulation.vi" Type="VI" URL="../Public/hpesgdxx Config Pulse Modulation.vi"/>
		<Item Name="hpesgdxx Config Pulse Step Increment.vi" Type="VI" URL="../Public/hpesgdxx Config Pulse Step Increment.vi"/>
		<Item Name="hpesgdxx Config Retrigger.vi" Type="VI" URL="../Public/hpesgdxx Config Retrigger.vi"/>
		<Item Name="hpesgdxx Config Sample Clock Rate.vi" Type="VI" URL="../Public/hpesgdxx Config Sample Clock Rate.vi"/>
		<Item Name="hpesgdxx Config Step Sweep.vi" Type="VI" URL="../Public/hpesgdxx Config Step Sweep.vi"/>
		<Item Name="hpesgdxx Config Sweep.vi" Type="VI" URL="../Public/hpesgdxx Config Sweep.vi"/>
		<Item Name="hpesgdxx Config Wideband CDMA.vi" Type="VI" URL="../Public/hpesgdxx Config Wideband CDMA.vi"/>
		<Item Name="hpesgdxx Create Sequence.vi" Type="VI" URL="../Public/hpesgdxx Create Sequence.vi"/>
		<Item Name="hpesgdxx Data Load.vi" Type="VI" URL="../Public/hpesgdxx Data Load.vi"/>
		<Item Name="hpesgdxx Delete Data.vi" Type="VI" URL="../Public/hpesgdxx Delete Data.vi"/>
		<Item Name="hpesgdxx Display.vi" Type="VI" URL="../Public/hpesgdxx Display.vi"/>
		<Item Name="hpesgdxx Dual ARB On_Off.vi" Type="VI" URL="../Public/hpesgdxx Dual ARB On_Off.vi"/>
		<Item Name="hpesgdxx Edit CDMA Channel Setup.vi" Type="VI" URL="../Public/hpesgdxx Edit CDMA Channel Setup.vi"/>
		<Item Name="hpesgdxx Edit CDMA2000 FW Channel Setup.vi" Type="VI" URL="../Public/hpesgdxx Edit CDMA2000 FW Channel Setup.vi"/>
		<Item Name="hpesgdxx Edit CDMA2000 REV Channel Setup.vi" Type="VI" URL="../Public/hpesgdxx Edit CDMA2000 REV Channel Setup.vi"/>
		<Item Name="hpesgdxx Edit User FIR.vi" Type="VI" URL="../Public/hpesgdxx Edit User FIR.vi"/>
		<Item Name="hpesgdxx Error Message.vi" Type="VI" URL="../Public/hpesgdxx Error Message.vi"/>
		<Item Name="hpesgdxx Error Query (Multiple).vi" Type="VI" URL="../Public/hpesgdxx Error Query (Multiple).vi"/>
		<Item Name="hpesgdxx Error Query.vi" Type="VI" URL="../Public/hpesgdxx Error Query.vi"/>
		<Item Name="hpesgdxx GSM Configure Hardware.vi" Type="VI" URL="../Public/hpesgdxx GSM Configure Hardware.vi"/>
		<Item Name="hpesgdxx GSM Freq Channels.vi" Type="VI" URL="../Public/hpesgdxx GSM Freq Channels.vi"/>
		<Item Name="hpesgdxx GSM Modify Standard.vi" Type="VI" URL="../Public/hpesgdxx GSM Modify Standard.vi"/>
		<Item Name="hpesgdxx GSM On_Off.vi" Type="VI" URL="../Public/hpesgdxx GSM On_Off.vi"/>
		<Item Name="hpesgdxx GSM Sync Out.vi" Type="VI" URL="../Public/hpesgdxx GSM Sync Out.vi"/>
		<Item Name="hpesgdxx Initialize.vi" Type="VI" URL="../Public/hpesgdxx Initialize.vi"/>
		<Item Name="hpesgdxx Load_Store Sweep List.vi" Type="VI" URL="../Public/hpesgdxx Load_Store Sweep List.vi"/>
		<Item Name="hpesgdxx ModOn_OFF.vi" Type="VI" URL="../Public/hpesgdxx ModOn_OFF.vi"/>
		<Item Name="hpesgdxx NADC Configure Hardware.vi" Type="VI" URL="../Public/hpesgdxx NADC Configure Hardware.vi"/>
		<Item Name="hpesgdxx NADC Freq Channels.vi" Type="VI" URL="../Public/hpesgdxx NADC Freq Channels.vi"/>
		<Item Name="hpesgdxx NADC Modify Standard.vi" Type="VI" URL="../Public/hpesgdxx NADC Modify Standard.vi"/>
		<Item Name="hpesgdxx NADC On_Off.vi" Type="VI" URL="../Public/hpesgdxx NADC On_Off.vi"/>
		<Item Name="hpesgdxx NADC Sync Out.vi" Type="VI" URL="../Public/hpesgdxx NADC Sync Out.vi"/>
		<Item Name="hpesgdxx PDC Configure Hardware.vi" Type="VI" URL="../Public/hpesgdxx PDC Configure Hardware.vi"/>
		<Item Name="hpesgdxx PDC Freq Channels.vi" Type="VI" URL="../Public/hpesgdxx PDC Freq Channels.vi"/>
		<Item Name="hpesgdxx PDC Modify Standard.vi" Type="VI" URL="../Public/hpesgdxx PDC Modify Standard.vi"/>
		<Item Name="hpesgdxx PDC On_Off.vi" Type="VI" URL="../Public/hpesgdxx PDC On_Off.vi"/>
		<Item Name="hpesgdxx PDC Sync Out.vi" Type="VI" URL="../Public/hpesgdxx PDC Sync Out.vi"/>
		<Item Name="hpesgdxx Query Registers.vi" Type="VI" URL="../Public/hpesgdxx Query Registers.vi"/>
		<Item Name="hpesgdxx Reset.vi" Type="VI" URL="../Public/hpesgdxx Reset.vi"/>
		<Item Name="hpesgdxx Retrigger.vi" Type="VI" URL="../Public/hpesgdxx Retrigger.vi"/>
		<Item Name="hpesgdxx Revision Query.vi" Type="VI" URL="../Public/hpesgdxx Revision Query.vi"/>
		<Item Name="hpesgdxx RF On_OFF.vi" Type="VI" URL="../Public/hpesgdxx RF On_OFF.vi"/>
		<Item Name="hpesgdxx Save Header.vi" Type="VI" URL="../Public/hpesgdxx Save Header.vi"/>
		<Item Name="hpesgdxx Save_Recall Secondary Frame.vi" Type="VI" URL="../Public/hpesgdxx Save_Recall Secondary Frame.vi"/>
		<Item Name="hpesgdxx Select CDMA2000 Link Type.vi" Type="VI" URL="../Public/hpesgdxx Select CDMA2000 Link Type.vi"/>
		<Item Name="hpesgdxx Self-Test.vi" Type="VI" URL="../Public/hpesgdxx Self-Test.vi"/>
		<Item Name="hpesgdxx Set Markers.vi" Type="VI" URL="../Public/hpesgdxx Set Markers.vi"/>
		<Item Name="hpesgdxx Set Phase Ref.vi" Type="VI" URL="../Public/hpesgdxx Set Phase Ref.vi"/>
		<Item Name="hpesgdxx Set Registers.vi" Type="VI" URL="../Public/hpesgdxx Set Registers.vi"/>
		<Item Name="hpesgdxx Set Signal Polarity.vi" Type="VI" URL="../Public/hpesgdxx Set Signal Polarity.vi"/>
		<Item Name="hpesgdxx Single Sweep.vi" Type="VI" URL="../Public/hpesgdxx Single Sweep.vi"/>
		<Item Name="hpesgdxx Store CDMA2000 Forward State.vi" Type="VI" URL="../Public/hpesgdxx Store CDMA2000 Forward State.vi"/>
		<Item Name="hpesgdxx Store CDMA2000 Reverse State.vi" Type="VI" URL="../Public/hpesgdxx Store CDMA2000 Reverse State.vi"/>
		<Item Name="hpesgdxx Store Custom CDMA State.vi" Type="VI" URL="../Public/hpesgdxx Store Custom CDMA State.vi"/>
		<Item Name="hpesgdxx Store Multicarrier CDMA2000 Forward State.vi" Type="VI" URL="../Public/hpesgdxx Store Multicarrier CDMA2000 Forward State.vi"/>
		<Item Name="hpesgdxx Store Multicarrier Custom CDMA State.vi" Type="VI" URL="../Public/hpesgdxx Store Multicarrier Custom CDMA State.vi"/>
		<Item Name="hpesgdxx Trigger.vi" Type="VI" URL="../Public/hpesgdxx Trigger.vi"/>
		<Item Name="hpesgdxx VI Tree.vi" Type="VI" URL="../Public/hpesgdxx VI Tree.vi"/>
		<Item Name="hpesgdxx Wideband CDMA On_Off.vi" Type="VI" URL="../Public/hpesgdxx Wideband CDMA On_Off.vi"/>
	</Item>
</Library>
